# Summer2021-No.141 openEuler社区体验地图的Web 3D小游戏开发

#### 介绍
https://gitee.com/openeuler-competition/summer-2021/issues/I3RJ5M  
项目借助游戏帮助openEuler社区用户熟悉openEuler各项功能  
**您可以在网页中输入 [210.22.22.150:4587](http://210.22.22.150:4587) 体验游戏**

#### 软件架构
软件架构说明
本项目基于cocos creator开发，开发了3D跑酷与2D解密两部分  
assets是项目资源文件夹  
euler-land_-package-main是服务器部署包体  
更多详细信息见结项报告  

#### web版本链接
url:[210.22.22.150:4587](http://210.22.22.150:4587)

